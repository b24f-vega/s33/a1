fetch("https://jsonplaceholder.typicode.com/todos",
{method:"GET"})
.then(response =>response.json())
.then(result => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos",
{method:"GET"})
.then(response =>response.json())
.then(result => {
    let title = result.map(element => element.title)
    console.log(title);
});

fetch("https://jsonplaceholder.typicode.com/todos/1",
{method:"GET"})
.then(response =>response.json())
.then(result => {
console.log(result)
});

fetch("https://jsonplaceholder.typicode.com/todos/",
    {   method:"POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"new item",
            completed:"false",
            userId:1
        })
})
.then(response => response.json()).then(result => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/10",{
    method:"PUT",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"updated  item",
            completed:"true",
            userId:1
        })
}).then(response => response.json()).then(result => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/10",{
    method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"clean roof",
            description:"remove tire and replace sealant",
            status:"Completed",
            dateCompleted:"04/7/2022",
            userId:"1"
        })
}).then(response => response.json()).then(result => console.log(result));

fetch("https://jsonplaceholder.typicode.com/todos/11",{
    method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            title:"updated title",
        })
}).then(response => response.json()).then(result => console.log(result));
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method:"PATCH",
        headers:{
            "Content-Type" : "application/json"
        },
        body:JSON.stringify({
            completed:"true",
            dateCompleted:"12/31/2022"
        })
}).then(response => response.json()).then(result => console.log(result));

fetch("https://jsonplaceholder.typicode.com/posts/99",{
    method:"DELETE"   
}).then(response => response.json()).then(result => console.log(result));